package controller;
import java.util.LinkedList;
import java.util.List;

import model.MYSQLDatabase;
import model.Rechteck;
import view.Eingabemaske;
public class BunteRechteckeController {
	
	private List<Rechteck> rechtecke;
	private MYSQLDatabase database;
	
	public BunteRechteckeController() {

	this.database = new MYSQLDatabase();
	rechtecke = this.database.getAlleRechtecke();
	}

	public static void main(String[] args) {
	

	}

	
	public void add(Rechteck rechteck) {
		rechtecke.add(rechteck);
		this.database.rechteckEintragen(rechteck);
	}
	public void reset() {
	rechtecke.clear();	
	}
	@Override
	public String toString() {
	//return "BunteRechteckeController [rechtecke=[Rechteck[x="+  +"";
		return "BunteRechteckeController [rechtecke=" + rechtecke.toString() + "]";
	}

	public List<Rechteck> getRechtecke() {
		return rechtecke;
	}

	public void setRechtecke(List<Rechteck> rechtecke) {
		this.rechtecke = rechtecke;
	}
	
	public void generiereZufallsRechtecke(int anzahl) {
	reset();
	for(int i = 0 ; i < 0 ; i++)
	rechtecke.add(Rechteck.generiereZufallsRechteck());
	}

	public void rechteckHinzufuegen() {
		new Eingabemaske(this).setVisible(true);
		
	}
}
