package model;

import java.util.Random;

public class Rechteck {
private int x;
private int y;
private int breite;
private int hoehe;


public Rechteck() {
this.x = 0;
this.y = 0;
this.hoehe = 0;
this.breite = 0;
}

public Rechteck(int x, int y, int breite, int hoehe) {
	this.x = x;
	this.y = y;
	if(breite < 0) {
	this.breite = breite * (-1);	
	}
	else {
	this.breite = breite;
	}
	if(hoehe < 0) {
	this.hoehe = hoehe * (-1);	
	}
	else {
	this.hoehe = hoehe;
	}
}

public int getX() {
	return x;
}

public void setX(int x) {
	this.x = x;
}

public int getY() {
	return y;
}

public void setY(int y) {
	this.y = y;
}

public int getBreite() {
	return breite;
}

public void setBreite(int breite) {
	this.breite = Math.abs(breite);
}

public int getHoehe() {
	return hoehe;
}

public void setHoehe(int hoehe) {
	this.hoehe = Math.abs(hoehe);
}

@Override
public String toString() {
	return "Rechteck [x=" + x + ", y=" + y + ", breite=" + breite + ", hoehe=" + hoehe + "]";
}

public boolean enthaelt(int x, int y) {
if( x == this.x && y == this.y) {
return true;	
}	
return false;	
}

public boolean enthaelt(Punkt P) {
return this.enthaelt(P.getX(), P.getY());	
}

public boolean enthaelt(Rechteck rechteck) {
if(rechteck.x > this.x && rechteck.breite < this.breite && rechteck.y > this.y && rechteck.hoehe < this.hoehe) {
	return true;
}
return false;
}


public static Rechteck generiereZufallsRechteck() {
Random random = new Random();	
Rechteck rechteck = new Rechteck();

rechteck.setX(random.nextInt(1200));
rechteck.setY(random.nextInt(1000));
rechteck.setHoehe(random.nextInt(1000 - rechteck.y));
rechteck.setBreite(random.nextInt(1200 - rechteck.x));
return rechteck;	
}
}