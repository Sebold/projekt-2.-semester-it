package test;

import model.Rechteck;
import controller.BunteRechteckeController;
public class RechteckTest {


public static void main(String args[]) {
BunteRechteckeController controllerobjekt = new BunteRechteckeController();	
	
Rechteck r0 = new Rechteck();
r0.setX(10);
r0.setY(10);
r0.setBreite(30);
r0.setHoehe(40);
Rechteck r1 = new Rechteck();
r1.setX(25);
r1.setY(25);
r1.setBreite(100);
r1.setHoehe(20);
Rechteck r2 = new Rechteck();
r2.setX(260);
r2.setY(10);
r2.setBreite(200);
r2.setHoehe(100);
Rechteck r3 = new Rechteck();
r3.setX(5);
r3.setY(500);
r3.setBreite(300);
r3.setHoehe(25);
Rechteck r4 = new Rechteck();
r4.setX(100);
r4.setY(100);
r4.setBreite(100);
r4.setHoehe(100);

Rechteck r5 = new Rechteck(200,200,200,200);
Rechteck r6 = new Rechteck(800,400,20,20);
Rechteck r7 = new Rechteck(800,450,20,20);
Rechteck r8 = new Rechteck(850,400,20,20);
Rechteck r9 = new Rechteck(855,455,25,25);

controllerobjekt.add(r0);
controllerobjekt.add(r1);
controllerobjekt.add(r2);
controllerobjekt.add(r3);
controllerobjekt.add(r4);
controllerobjekt.add(r5);
controllerobjekt.add(r6);
controllerobjekt.add(r7);
controllerobjekt.add(r8);
controllerobjekt.add(r9);
System.out.println(controllerobjekt.toString());

//Pr�fen ob Rechteckerstellung mit negativen Breiten und H�hen m�glich ist
Rechteck eck10 = new Rechteck(-4,-5,-50,-200);
System.out.println(eck10); //Rechteck [x=-4, y=-5, breite=50, hoehe=200]
Rechteck eck11 = new Rechteck();
eck11.setX(-10);
eck11.setY(-10);
eck11.setBreite(-200);
eck11.setHoehe(-100);
System.out.println(eck11);
//Rechteck [x=-10, y=-10, breite=200, hoehe=100]
Rechteck rtest = new Rechteck(30, 30, 90, 15);
System.out.println(r1.enthaelt(rtest));

Rechteck rtest2 = new Rechteck();
rtest2 = Rechteck.generiereZufallsRechteck();
System.out.println(rtest2);
rechteckeTesten();
}

public static void rechteckeTesten() {
Rechteck rtest3 = new Rechteck(0,0,1200,1000);
Rechteck[] rechteckliste = new Rechteck[50000];
for(int i = 0; i < 50000; i++) {
rechteckliste[i] = Rechteck.generiereZufallsRechteck();
System.out.println(rtest3.enthaelt(rechteckliste[i]));

}


}

}
