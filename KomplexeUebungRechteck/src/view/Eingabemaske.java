package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.BunteRechteckeController;
import model.Rechteck;

import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.GridLayout;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Eingabemaske extends JFrame {
	
	BunteRechteckeController brc ;
	private JPanel contentPane;
	private JTextField tfd_xkoordinate;
	private JTextField tfd_h�henwert;
	private JTextField tfd_ycoordinate;
	private JTextField tfd_breitenwert;

	

	/**
	 * Create the frame.
	 * @param bunteRechteckeController 
	 */
	public Eingabemaske(BunteRechteckeController bunteRechteckeController) {
		this.brc = bunteRechteckeController;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(5, 10));
		
		
		JLabel lbl_title = new JLabel("Ein Rechteck hinzuf\u00FCgen");
		lbl_title.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lbl_title, BorderLayout.NORTH);
		
		JPanel pnl_rechteckinfos = new JPanel();
		contentPane.add(pnl_rechteckinfos, BorderLayout.CENTER);
		pnl_rechteckinfos.setLayout(new GridLayout(4, 2, 5, 20));
		
		JLabel lbl_xkoordinate = new JLabel("X Koordinate");
		pnl_rechteckinfos.add(lbl_xkoordinate);
		
		tfd_xkoordinate = new JTextField();
		pnl_rechteckinfos.add(tfd_xkoordinate);
		tfd_xkoordinate.setColumns(10);
		
		JLabel lbl_ycoordinate = new JLabel("Y Koordinate");
		pnl_rechteckinfos.add(lbl_ycoordinate);
		
		tfd_ycoordinate = new JTextField();
		pnl_rechteckinfos.add(tfd_ycoordinate);
		tfd_ycoordinate.setColumns(10);
		
		JLabel lbl_h�henwert = new JLabel("H\u00F6henwert");
		pnl_rechteckinfos.add(lbl_h�henwert);
		
		tfd_h�henwert = new JTextField();
		pnl_rechteckinfos.add(tfd_h�henwert);
		tfd_h�henwert.setColumns(10);
		
		JLabel lbl_breitenwert = new JLabel("Breitenwert");
		pnl_rechteckinfos.add(lbl_breitenwert);
		
		tfd_breitenwert = new JTextField();
		pnl_rechteckinfos.add(tfd_breitenwert);
		tfd_breitenwert.setColumns(10);
		
		JButton btn_einf�gen = new JButton("Rechteck einf\u00FCgen");
		btn_einf�gen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			Rechteck rechteck1 = new Rechteck();	
			rechteck1.setX(Integer.parseInt(tfd_xkoordinate.getText()));
			rechteck1.setY(Integer.parseInt(tfd_ycoordinate.getText()));
			rechteck1.setHoehe(Integer.parseInt(tfd_h�henwert.getText()));
			rechteck1.setBreite(Integer.parseInt(tfd_breitenwert.getText()));
			brc.add(rechteck1);
			}
		});
		contentPane.add(btn_einf�gen, BorderLayout.SOUTH);
	}

}
