package view;

import controller.*;
import model.*;

import java.awt.Color;
import java.awt.Graphics;


import javax.swing.JPanel;

public class Zeichenflaeche extends JPanel {
private BunteRechteckeController brc;


public Zeichenflaeche(final  BunteRechteckeController brc) {
	this.brc = brc;
	}

	
	@Override
	public void paintComponent(Graphics g) {
	g.setColor(Color.BLACK);
	for(Rechteck rechtecke: brc.getRechtecke()) {
	g.drawRect(rechtecke.getX(), rechtecke.getY(), rechtecke.getBreite(), rechtecke.getHoehe());	
	}
	
	
	}

}
